const express = require('express');
const cors = require('cors');
const helmet = require('helmet');
const compression = require('compression');
require('express-async-errors');

const { NoFoundMiddleware, ErrorMiddleware }  = require('../middlewares');

module.exports = function({HomeRoutes}){
    const router = express.Router();
    const apiRoutes = express.Router();
    //Middlewares
    apiRoutes.use(express.json());
    apiRoutes.use(cors());
    apiRoutes.use(helmet());
    apiRoutes.use(compression());

    apiRoutes.use('/home', HomeRoutes);

    router.use('/v1/api', apiRoutes);

    //Middlewares logicos

    router.use(NoFoundMiddleware);
    router.use(ErrorMiddleware);

    return router;
}