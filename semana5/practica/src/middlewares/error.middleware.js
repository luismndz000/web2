module.exports = ( err, req, res, next ) => 
{
    const httpStatus = err.status || 500;

    const httpMessage = err.message || 'Ups, something happend...';

    return req.status(httpStatus).send({
        status: httpStatus,
        message: httpMessage
    })

}