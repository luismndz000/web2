module.exports = {
    NoFoundMiddleware: require('./404.middleware'),
    ErrorMiddleware: require('./error.middleware')
}