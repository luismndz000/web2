const express = require("express");
const app = express();
const mongoose = require("mongoose");

const { MONGO_URI } = require("./config");
const { Noticias } = require("./models");
app.use(express.json())
//conexion a la base de datos
mongoose.connect(MONGO_URI, { useNewUrlParser:true, useUnifiedTopology:true } );

// peticion get que devuelve la base de datos 
app.get('/',  (req,res) => {
        Noticias.find({},(err,docs)=>{        
        res.send({
            docs
        })
        
    });
    
})

app.listen(3000, ()=>{
    console.log(`El servidor esta ejecutandose en el puerto 3000`)
})