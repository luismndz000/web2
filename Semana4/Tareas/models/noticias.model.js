const mongoose = require("mongoose")
const {Schema} = mongoose;

const NoticiasSchema = new Schema//separo el schema del modelo
(
    {
    titulo: { type: String },
    enlace: { type: String }
    },
    {
        timestamps: { createdAt: true, updatedAt: true}//crea marca de tiempo cuando se crea y actualise el documento
    }
)
module.exports = mongoose.model("Noticias", NoticiasSchema );//defino e importo el modelo

