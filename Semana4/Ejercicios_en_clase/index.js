//Modules
const express = require('express');
const cors = require('cors');
const path = require('path');

//Server
const app = express();
const PORT = 3000;

//Middlewares
app.use(cors());
app.use(express.json());
app.use('/public', express.static(path.join(__dirname,'./public')));

let students = [{name: 'Luis Méndez', course: 'WEB II A'}];

/*=====Routes=====*/

//get
app.get('/', (req,res) => {
    res.status(200).send(students);
});

//get?id
app.get('/:name', (req,res) => {
    const { name } = req.params;
    const student = students.find( student => student.name === name );

    if(student) return res.status(200).send(student);

    return res.status(400).send({msg: "No se encontró el estudiante"});
    
});

//post
app.post('/', (req,res) => {
    const { name,course } = req.body;
    students.push({name,course});
    res.status(201).send({
        msg: "Estudiante agregado correctamente",
        student: {name,course}
    });
} );

//put
app.put('/:name', (req,res) => {
    const find_name = req.params.name;
    const { name,course } = req.body;
    const student_index = students.findIndex( student => student.name === find_name );
    students[student_index].name = name;
    students[student_index].course = course;
    res.send({
        msg: "ok"
    });
});

//delete
app.delete('/:name', (req,res) => {
    const { name } = req.params;
    const student = students.findIndex( student => student.name === name );
    res.send({
        msg: "ok"
    });
});

//Start server
app.listen(PORT, () => console.log('The server has started in http://127.0.0.1:' + PORT));